package com.sms.controllers;

import com.sms.constants.ResponseTagName;
import com.sms.documents.User;
import com.sms.services.UserService;
import com.sms.util.BcryptHashGenerator;
import java.util.LinkedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/user/log", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class LoginController {

    @Autowired
    private UserService userService;

    @PostMapping("/web/login")
    ResponseEntity<?> webLogin(@RequestBody User user) {
        LinkedHashMap<String, Object> serviceResponse = new LinkedHashMap<String, Object>();
        User u = userService.findUserByUserName(user.getUserName());
        if (u != null) {
            if (BcryptHashGenerator.checkPassword(user.getPassword(), u.getPassword())) {
                serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
                serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                serviceResponse.put(ResponseTagName.MESSAGE, "Login successfull");
                serviceResponse.put(ResponseTagName.USER, u);
                return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
            } else {
                serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
                serviceResponse.put(ResponseTagName.STATUS_CODE, 201);
                serviceResponse.put(ResponseTagName.MESSAGE, "User name or password invalid");
                return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
            }
        } else {
            serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
            serviceResponse.put(ResponseTagName.STATUS_CODE, 201);
            serviceResponse.put(ResponseTagName.MESSAGE, "User name or password invalid");
            return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
        }
    }
}
