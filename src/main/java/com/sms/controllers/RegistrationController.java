package com.sms.controllers;

import com.sms.constants.ResponseTagName;
import com.sms.documents.Institute;
import com.sms.documents.User;
import com.sms.documents.UserRole;
import com.sms.services.InstituteService;
import com.sms.services.UserRoleService;
import com.sms.services.UserService;
import com.sms.util.ErrorUtils;
import java.util.LinkedHashMap;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/user/rg", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService roleService;

    @Autowired
    private InstituteService instituteService;

    @PostMapping("/web/registration")
    ResponseEntity<?> webRegistration(@Valid @RequestBody User user, BindingResult result) {
        LinkedHashMap<String, Object> serviceResponse = new LinkedHashMap<String, Object>();
        if (result.hasErrors()) {
            return new ResponseEntity<>(ErrorUtils.userError(result), new HttpHeaders(), HttpStatus.BAD_REQUEST);
        } else {
            User phone = userService.findUserByPhone(user.getPhone());
            if (phone == null) {
                User uName = userService.findUserByUserName(user.getUserName());
                if (uName == null) {
                    UserRole uRole = roleService.findUserRoleById(user.getUserRole().getId());
                    if (uRole != null) {
                        user.getUserRole().setUserRole(uRole.getUserRole());
                        Institute institute = instituteService.findInstituteById(user.getInstitute().getId());
                        if (institute != null) {
                            user.getInstitute().setInstituteName(institute.getInstituteName());
                            User u = userService.save(user);
                            if (u != null) {
                                serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
                                serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                                serviceResponse.put(ResponseTagName.MESSAGE, "Registration has been successfully done.");
                                serviceResponse.put(ResponseTagName.USER, u);
                                return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
                            } else {
                                serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                                serviceResponse.put(ResponseTagName.STATUS_CODE, 400);
                                serviceResponse.put(ResponseTagName.MESSAGE, "Registration has been failed.");
                                return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
                            }
                        } else {
                            serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                            serviceResponse.put(ResponseTagName.STATUS_CODE, 400);
                            serviceResponse.put(ResponseTagName.MESSAGE, "Invalid Institute.");
                            return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
                        }
                    } else {
                        serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                        serviceResponse.put(ResponseTagName.STATUS_CODE, 400);
                        serviceResponse.put(ResponseTagName.MESSAGE, "Invalid user role.");
                        return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
                    }
                } else {
                    serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                    serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                    serviceResponse.put(ResponseTagName.MESSAGE, "User name exist.");
                    serviceResponse.put(ResponseTagName.IS_EXIST, Boolean.FALSE);
                    return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
                }
            } else {
                serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                serviceResponse.put(ResponseTagName.MESSAGE, "Phone exist.");
                serviceResponse.put(ResponseTagName.IS_EXIST, Boolean.FALSE);
                return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
            }
        }
    }
}
