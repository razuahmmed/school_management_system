package com.sms.controllers;

import com.sms.constants.ResponseTagName;
import com.sms.documents.User;
import com.sms.services.UserService;
import java.util.LinkedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/user/find", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class FindUserController {

    @Autowired
    private UserService userService;

    @GetMapping("/web/by/phone")
    ResponseEntity<?> findUserByPhone(@RequestParam(name = "phone", required = true) String phone) {
        LinkedHashMap<String, Object> serviceResponse = new LinkedHashMap<String, Object>();
        User u = userService.findUserByPhone(phone);
        if (u != null) {
            serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
            serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
            serviceResponse.put(ResponseTagName.MESSAGE, "Phone exist.");
            serviceResponse.put(ResponseTagName.USER, u);
            serviceResponse.put(ResponseTagName.IS_EXIST, Boolean.TRUE);
            return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
        } else {
            serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
            serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
            serviceResponse.put(ResponseTagName.MESSAGE, "Phone not exist.");
            serviceResponse.put(ResponseTagName.IS_EXIST, Boolean.FALSE);
            return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/web/by/userName")
    ResponseEntity<?> findUserByUserName(@RequestParam(name = "userName", required = true) String userName) {
        LinkedHashMap<String, Object> serviceResponse = new LinkedHashMap<String, Object>();
        User u = userService.findUserByUserName(userName);
        if (u != null) {
            serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
            serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
            serviceResponse.put(ResponseTagName.MESSAGE, "User name exist.");
            serviceResponse.put(ResponseTagName.USER, u);
            serviceResponse.put(ResponseTagName.IS_EXIST, Boolean.TRUE);
            return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
        } else {
            serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
            serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
            serviceResponse.put(ResponseTagName.MESSAGE, "User name not exist.");
            serviceResponse.put(ResponseTagName.IS_EXIST, Boolean.FALSE);
            return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
}
