package com.sms.controllers;

import com.sms.constants.ResponseTagName;
import com.sms.documents.Institute;
import com.sms.documents.InstituteStatus;
import com.sms.documents.InstituteType;
import com.sms.services.InstituteService;
import com.sms.services.InstituteStatusService;
import com.sms.services.InstituteTypeService;
import com.sms.util.ErrorUtils;
import java.util.LinkedHashMap;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/institute", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class InstituteController {

    @Autowired
    private InstituteTypeService instituteTypeService;

    @Autowired
    private InstituteStatusService instituteStatusService;

    @Autowired
    private InstituteService instituteService;

    @PostMapping("/type/create")
    ResponseEntity<?> createInstituteType(@Valid @RequestBody InstituteType type, BindingResult result) {
        LinkedHashMap<String, Object> serviceResponse = new LinkedHashMap<String, Object>();
        if (result.hasErrors()) {
            return new ResponseEntity<>(ErrorUtils.userError(result), new HttpHeaders(), HttpStatus.BAD_REQUEST);
        } else {
            InstituteType types = instituteTypeService.findInstituteTypeByName(type.getInstituteType());
            if (types == null) {
                InstituteType t = instituteTypeService.save(type);
                if (t != null) {
                    serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
                    serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                    serviceResponse.put(ResponseTagName.MESSAGE, "Institute Type create has been successfully done.");
                    serviceResponse.put(ResponseTagName.INSTITUTE_TYPE, t);
                    return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
                } else {
                    serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                    serviceResponse.put(ResponseTagName.STATUS_CODE, 400);
                    serviceResponse.put(ResponseTagName.MESSAGE, "Institute Type create has been failed.");
                    return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
                }
            } else {
                serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                serviceResponse.put(ResponseTagName.MESSAGE, "Institute Type exist.");
                return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
            }
        }
    }

    @PostMapping("/status/create")
    ResponseEntity<?> createInstituteStatus(@Valid @RequestBody InstituteStatus status, BindingResult result) {
        LinkedHashMap<String, Object> serviceResponse = new LinkedHashMap<String, Object>();
        if (result.hasErrors()) {
            return new ResponseEntity<>(ErrorUtils.userError(result), new HttpHeaders(), HttpStatus.BAD_REQUEST);
        } else {
            InstituteStatus st = instituteStatusService.findInstituteByName(status.getInstituteStatus());
            if (st == null) {
                InstituteStatus sts = instituteStatusService.save(status);
                if (sts != null) {
                    serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
                    serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                    serviceResponse.put(ResponseTagName.MESSAGE, "Institute Status create has been successfully done.");
                    serviceResponse.put(ResponseTagName.INSTITUTE_STATUS, sts);
                    return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
                } else {
                    serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                    serviceResponse.put(ResponseTagName.STATUS_CODE, 400);
                    serviceResponse.put(ResponseTagName.MESSAGE, "Institute Status create has been failed.");
                    return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
                }
            } else {
                serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                serviceResponse.put(ResponseTagName.MESSAGE, "Institute Status exist.");
                return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
            }
        }
    }

    @PostMapping("/create/institution")
    ResponseEntity<?> createInstitution(@Valid @RequestBody Institute institute, BindingResult result) {
        LinkedHashMap<String, Object> serviceResponse = new LinkedHashMap<String, Object>();
        if (result.hasErrors()) {
            return new ResponseEntity<>(ErrorUtils.userError(result), new HttpHeaders(), HttpStatus.BAD_REQUEST);
        } else {
            InstituteType type = instituteTypeService.findInstituteTypeById(institute.getInstituteType().getId());
            if (type != null) {
                institute.getInstituteType().setInstituteType(type.getInstituteType());
                InstituteStatus status = instituteStatusService.findInstituteById(institute.getInstituteStatus().getId());
                if (status != null) {
                    institute.getInstituteStatus().setInstituteStatus(status.getInstituteStatus());
                    Institute i = instituteService.save(institute);
                    if (i != null) {
                        serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
                        serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                        serviceResponse.put(ResponseTagName.MESSAGE, "Institution create has been successfully done.");
                        serviceResponse.put(ResponseTagName.INSTITUTION, i);
                        return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
                    } else {
                        serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                        serviceResponse.put(ResponseTagName.STATUS_CODE, 400);
                        serviceResponse.put(ResponseTagName.MESSAGE, "Institution create has been failed.");
                        return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
                    }
                } else {
                    serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                    serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                    serviceResponse.put(ResponseTagName.MESSAGE, "Institute Status invalid");
                    return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
                }
            } else {
                serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                serviceResponse.put(ResponseTagName.MESSAGE, "Institute Type invalid");
                return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
            }
        }
    }

    @GetMapping("/get/all/institution")
    ResponseEntity<?> getAllInstitution(Pageable pageable) {
        LinkedHashMap<String, Object> serviceResponse = new LinkedHashMap<String, Object>();
        List<Institute> institutes = (List<Institute>) instituteService.findAllInstitute(pageable);
        if (!institutes.isEmpty()) {
            serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
            serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
            serviceResponse.put(ResponseTagName.MESSAGE, "All Institution.");
            serviceResponse.put(ResponseTagName.INSTITUTION_LIST, institutes);
            return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
        } else {
            serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
            serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
            serviceResponse.put(ResponseTagName.MESSAGE, "Institution not found.");
            return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
        }
    }
}
