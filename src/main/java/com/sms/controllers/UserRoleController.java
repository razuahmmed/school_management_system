package com.sms.controllers;

import com.sms.constants.ResponseTagName;
import com.sms.documents.UserRole;
import com.sms.services.UserRoleService;
import com.sms.util.ErrorUtils;
import java.util.LinkedHashMap;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/user/role", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRoleController {

    @Autowired
    private UserRoleService userRoleService;

    @PostMapping("/create")
    ResponseEntity<?> createUserRole(@Valid @RequestBody UserRole role, BindingResult result) {
        LinkedHashMap<String, Object> serviceResponse = new LinkedHashMap<String, Object>();
        if (result.hasErrors()) {
            return new ResponseEntity<>(ErrorUtils.userError(result), new HttpHeaders(), HttpStatus.BAD_REQUEST);
        } else {
            UserRole r = userRoleService.findUserRoleByName(role.getUserRole());
            if (r == null) {
                UserRole roles = userRoleService.save(role);
                if (roles != null) {
                    serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
                    serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                    serviceResponse.put(ResponseTagName.MESSAGE, "User Role create has been successfully done.");
                    serviceResponse.put(ResponseTagName.USER_ROLE, roles);
                    return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.OK);
                } else {
                    serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                    serviceResponse.put(ResponseTagName.STATUS_CODE, 400);
                    serviceResponse.put(ResponseTagName.MESSAGE, "User Role create has been failed.");
                    return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
                }
            } else {
                serviceResponse.put(ResponseTagName.STATUS, Boolean.FALSE);
                serviceResponse.put(ResponseTagName.STATUS_CODE, 200);
                serviceResponse.put(ResponseTagName.MESSAGE, "User Role exist.");
                return new ResponseEntity<>(serviceResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
            }
        }
    }
}
