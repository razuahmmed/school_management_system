package com.sms.constants;

public final class ResponseTagName {

    public static final String API_RESPONSE = "apiresponse";
    public static final String STATUS = "status";
    public static final String STATUS_CODE = "statusCode";
    public static final String USER = "user";
    public static final String IS_EXIST = "isExist";
    public static final String PHONE = "phone";

    public static final String ID = "id";
    public static final String CREATED = "created";
    public static final String TOTAL = "total";
    public static final String UPDATE = "update";
    public static final String DELETED = "deleted";
    public static final String RETRIEVED = "retrieved";
    public static final String LIMIT = "limit";
    public static final String LIST = "list";
    public static final String MESSAGE = "message";
    public static final String NAME_EMAIL = "email";

    public static final String SUCCESS = "success";
    public static final String USERID = "userId";
    public static final String USER_INFO = "userInfo";
    public static final String USER_EMAIL = "email";
    public static final String PROFILE_AVATAR = "avatarUrl";
    public static final String DOCUMENT = "document";

    public static final String USER_ROLE = "user_role";
    public static final String INSTITUTE_TYPE = "institute_type";
    public static final String INSTITUTE_STATUS = "institute_status";
    public static final String INSTITUTION = "institution";
    public static final String INSTITUTION_LIST = "institution_list";

}
