package com.sms.util;

import com.sms.constants.ResponseTagName;
import java.util.LinkedHashMap;
import org.springframework.validation.BindingResult;

public class ErrorUtils {

    public static LinkedHashMap userError(BindingResult result) {
        LinkedHashMap serviceResponse = new LinkedHashMap<String, Object>();
        serviceResponse.put(ResponseTagName.STATUS, Boolean.TRUE);
        serviceResponse.put(ResponseTagName.STATUS_CODE, 201);
        serviceResponse.put(ResponseTagName.MESSAGE, result.getAllErrors().get(0).getDefaultMessage());
        return serviceResponse;
    }
}
