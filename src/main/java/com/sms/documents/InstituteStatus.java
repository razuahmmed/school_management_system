package com.sms.documents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sms.entities.BaseEntity;
import com.sms.util.PatternConstant;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.data.mongodb.core.mapping.Document;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "institute_status")
public class InstituteStatus extends BaseEntity {

    @NotNull(message = "Please enter Institute Status")
    @Size(min = 3, max = 150, message = "Please enter Institute Status between {min} to {max} characters")
    @Pattern(regexp = PatternConstant.ID_PATTERN, message = "Please enter only characters in Institute Status")
    private String instituteStatus;

    public String getInstituteStatus() {
        return instituteStatus;
    }

    public void setInstituteStatus(String instituteStatus) {
        this.instituteStatus = instituteStatus;
    }
}
