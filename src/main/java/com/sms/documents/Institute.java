package com.sms.documents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sms.entities.BaseEntity;
import com.sms.util.PatternConstant;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.data.mongodb.core.mapping.Document;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "institute_info")
public class Institute extends BaseEntity {

    @NotNull(message = "Please enter Institute Name")
    @Size(min = 3, max = 150, message = "Please enter Institute Name between {min} to {max} characters")
    @Pattern(regexp = PatternConstant.CHAR_PATTERN, message = "Please enter only characters in Institute Name")
    private String instituteName;
    private InstituteType instituteType;
    private InstituteStatus instituteStatus;

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public InstituteType getInstituteType() {
        return instituteType;
    }

    public void setInstituteType(InstituteType instituteType) {
        this.instituteType = instituteType;
    }

    public InstituteStatus getInstituteStatus() {
        return instituteStatus;
    }

    public void setInstituteStatus(InstituteStatus instituteStatus) {
        this.instituteStatus = instituteStatus;
    }
}
