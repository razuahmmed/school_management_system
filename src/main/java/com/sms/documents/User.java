package com.sms.documents;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sms.entities.Address;
import com.sms.entities.BaseEntity;
import com.sms.util.PatternConstant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "user_info")
public class User extends BaseEntity implements UserDetails {

    @NotNull(message = "Please enter Phone")
    @Size(min = 10, max = 11, message = "Please enter Phone between {min} to {max} characters")
    @Pattern(regexp = PatternConstant.MOBILE_PATTERN, message = "Please enter only digit in Phone Number")
    private String phone;

    @NotNull(message = "Please enter User Name")
    @Size(min = 3, max = 30, message = "Please enter User Name between {min} to {max} characters")
    @Pattern(regexp = PatternConstant.ID_PATTERN, message = "Please enter only characters in User Name")
    private String userName;

    @NotNull(message = "Please enter First Name")
    @Size(min = 3, max = 15, message = "Please enter First Name between {min} to {max} characters")
    @Pattern(regexp = PatternConstant.CHAR_PATTERN, message = "Please enter only characters in First Name")
    private String firstName;

    @NotNull(message = "Please enter Last Name")
    @Size(min = 3, max = 15, message = "Please enter Last Name between {min} to {max} characters")
    @Pattern(regexp = PatternConstant.CHAR_PATTERN, message = "Please enter only characters in Last Name")
    private String lastName;

    @Size(min = 3, max = 30, message = "Please enter Email between {min} to {max} characters")
    @Pattern(regexp = PatternConstant.EMAIL_PATTERN, message = "Please enter valid Email format")
    private String email;

    @NotNull(message = "Please enter Password")
    @Size(min = 6, max = 30, message = "Please enter Password between {min} to {max} characters")
    @Pattern(regexp = PatternConstant.ID_PATTERN, message = "Only characters and digit allow in password")
    private String password;

    @JsonIgnore
    @Transient
    private List<GrantedAuthority> authority = AuthorityUtils.createAuthorityList(new String[]{"ROLE_USER"});

    private Address address;
    private UserRole userRole;
    private Institute institute;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public Institute getInstitute() {
        return institute;
    }

    public void setInstitute(Institute institute) {
        this.institute = institute;
    }

    /**
     * ***********************************
     *
     * @return
     */
    public List<GrantedAuthority> getAuthority() {
        return authority;
    }

    public void setAuthority(List<GrantedAuthority> authority) {
        this.authority = authority;
    }

    @Override
    @JsonIgnore
    public List<GrantedAuthority> getAuthorities() {
        return this.authority;
    }

    @JsonIgnore
    public void setAuthorities(String authority) {
        List<String> roles = new ArrayList<>();
        for (GrantedAuthority grantedAuthority : this.authority) {
            roles.add(grantedAuthority.getAuthority());
        }
        if (authority.length() > 0) {
            roles.add(authority);
        }
        this.authority = AuthorityUtils.createAuthorityList(roles.toArray(new String[roles.size()]));
    }

    @JsonIgnore
    public boolean hasRole(String role) {
        Collection<? extends GrantedAuthority> authorities = this.authority;
        return authorities.contains(new SimpleGrantedAuthority(role));

    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    /**
     * ******************************
     *
     */
}
