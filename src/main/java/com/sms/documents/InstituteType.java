package com.sms.documents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sms.entities.BaseEntity;
import com.sms.util.PatternConstant;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.data.mongodb.core.mapping.Document;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "institute_type")
public class InstituteType extends BaseEntity {

    @NotNull(message = "Please enter Institute Type")
    @Size(min = 3, max = 150, message = "Please enter Institute Type between {min} to {max} characters")
    @Pattern(regexp = PatternConstant.CHAR_PATTERN, message = "Please enter only characters in Institute Type")
    private String instituteType;

    public String getInstituteType() {
        return instituteType;
    }

    public void setInstituteType(String instituteType) {
        this.instituteType = instituteType;
    }
}
