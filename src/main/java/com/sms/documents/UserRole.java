package com.sms.documents;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sms.entities.BaseEntity;
import com.sms.util.PatternConstant;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.data.mongodb.core.mapping.Document;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "user_role")
public class UserRole extends BaseEntity {

    @NotNull(message = "Please enter User Role")
    @Size(min = 3, max = 150, message = "Please enter User Role between {min} to {max} characters")
    @Pattern(regexp = PatternConstant.CHAR_PATTERN, message = "Please enter only characters in User Role")
    private String userRole;

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}
