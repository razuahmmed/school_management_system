package com.sms.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.SafeHtml;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {

    @Size(min = 0, max = 100, message = "Your address road must be less than {max} characters.")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE,
            message = "Please make sure your address road is properly formatted, containing no malicious characters.")
    private String road;

    @Size(min = 0, max = 100, message = "Your address city must be less than {max} characters.")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE,
            message = "Please make sure address city is properly formatted, containing no malicious characters.")
    private String city;

    @Size(min = 0, max = 100, message = "Your country be less than {max} characters.")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE,
            message = "Please make sure 'Your country' is properly formatted, containing no malicious characters.")
    private String country;

    @Size(min = 0, max = 100, message = "Your zip be less than {max} characters.")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE,
            message = "Please make sure 'Your zip' is properly formatted, containing no malicious characters.")
    private String zip;

    @Size(min = 0, max = 15, message = "Your fax be in {max} didits.")
    @Pattern(regexp = "[0-9\\-., ]*", message = "Your fax requires valid numbers and characters.")
    @SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE,
            message = "Please make sure 'Your fax' is properly formatted, containing no malicious characters.")
    private String fax;

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}
