package com.sms.repositories;

import com.sms.documents.Institute;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InstituteRepository extends MongoRepository<Institute, String> {

    Institute findOneByInstituteName(String instituteName);
}
