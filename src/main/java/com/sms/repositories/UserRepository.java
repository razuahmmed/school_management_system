package com.sms.repositories;

import com.sms.documents.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    User findOneByPhone(String phone);

    User findOneByUserName(String userName);
}
