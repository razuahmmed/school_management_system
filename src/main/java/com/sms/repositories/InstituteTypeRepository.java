package com.sms.repositories;

import com.sms.documents.InstituteType;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InstituteTypeRepository extends MongoRepository<InstituteType, String> {

    InstituteType findOneByInstituteType(String instituteType);
}
