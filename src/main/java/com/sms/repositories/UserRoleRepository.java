package com.sms.repositories;

import com.sms.documents.UserRole;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRoleRepository extends MongoRepository<UserRole, String> {

    UserRole findOneByUserRole(String userRole);
}
