package com.sms.repositories;

import com.sms.documents.InstituteStatus;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InstituteStatusRepository extends MongoRepository<InstituteStatus, String> {

    InstituteStatus findOneByInstituteStatus(String instituteStatus);
}
