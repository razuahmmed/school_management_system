package com.sms.services.impl;

import com.sms.documents.InstituteStatus;
import com.sms.repositories.InstituteStatusRepository;
import com.sms.services.InstituteStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class InstituteStatusServiceImpl implements InstituteStatusService {

    @Autowired
    private InstituteStatusRepository repository;

    @Override
    public Iterable<InstituteStatus> findAllInstitute(Pageable pageable) {
        return repository.findAll(pageable).getContent();
    }

    @Override
    public InstituteStatus findInstituteById(String id) {
        return repository.findOne(id);
    }

    @Override
    public InstituteStatus findInstituteByName(String name) {
        return repository.findOneByInstituteStatus(name);
    }

    @Override
    public InstituteStatus save(InstituteStatus status) {
        return repository.save(status);
    }

    @Override
    public InstituteStatus update(InstituteStatus status) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
