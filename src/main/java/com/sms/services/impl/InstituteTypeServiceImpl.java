package com.sms.services.impl;

import com.sms.documents.InstituteType;
import com.sms.repositories.InstituteTypeRepository;
import com.sms.services.InstituteTypeService;
import com.sms.util.DateFormatProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class InstituteTypeServiceImpl implements InstituteTypeService {

    @Autowired
    private InstituteTypeRepository repository;

    @Override
    public Iterable<InstituteType> findAllInstituteType(Pageable pageable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public InstituteType findInstituteTypeById(String id) {
        return repository.findOne(id);
    }

    @Override
    public InstituteType findInstituteTypeByName(String type) {
        return repository.findOneByInstituteType(type);
    }

    @Override
    public InstituteType save(InstituteType type) {
        type.setCreatedDate(DateFormatProvider.now());
        type.setCreatedBy("user id");
        return repository.save(type);
    }

    @Override
    public InstituteType update(InstituteType user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
