package com.sms.services.impl;

import com.sms.documents.User;
import com.sms.repositories.UserRepository;
import com.sms.services.UserService;
import com.sms.util.DateFormatProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Iterable<User> findAllUser(Pageable pageable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User findUserById(String id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findUserByUserName(String uName) {
        return userRepository.findOneByUserName(uName);
    }

    @Override
    public User findUserByPhone(String phone) {
        return userRepository.findOneByPhone(phone);
    }

    @Override
    public User save(User user) {
        user.setCreatedDate(DateFormatProvider.now());
        user.setCreatedBy("user id");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public User update(User user) {
        return userRepository.save(user);
    }

    @Override
    public Boolean delete(String id) {
        userRepository.delete(id);
        return !userRepository.exists(id);
    }
}
