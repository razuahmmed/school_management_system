package com.sms.services.impl;

import com.sms.documents.Institute;
import com.sms.repositories.InstituteRepository;
import com.sms.services.InstituteService;
import com.sms.util.DateFormatProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class InstituteServiceImpl implements InstituteService {

    @Autowired
    private InstituteRepository repository;

    @Override
    public Iterable<Institute> findAllInstitute(Pageable pageable) {
        return repository.findAll(pageable).getContent();
    }

    @Override
    public Institute findInstituteById(String id) {
        return repository.findOne(id);
    }

    @Override
    public Institute findInstituteByName(String name) {
        return repository.findOneByInstituteName(name);
    }

    @Override
    public Institute save(Institute institute) {
        institute.setCreatedDate(DateFormatProvider.now());
        institute.setCreatedBy("user id");
        return repository.save(institute);
    }

    @Override
    public Institute update(Institute institute) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
