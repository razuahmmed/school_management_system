package com.sms.services.impl;

import com.sms.documents.UserRole;
import com.sms.repositories.UserRoleRepository;
import com.sms.services.UserRoleService;
import com.sms.util.DateFormatProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleRepository repository;

    @Override
    public Iterable<UserRole> findAllUserRole(Pageable pageable) {
        return repository.findAll(pageable).getContent();
    }

    @Override
    public UserRole findUserRoleById(String id) {
        return repository.findOne(id);
    }

    @Override
    public UserRole findUserRoleByName(String uRole) {
        return repository.findOneByUserRole(uRole);
    }

    @Override
    public UserRole save(UserRole userRole) {
        userRole.setCreatedDate(DateFormatProvider.now());
        userRole.setCreatedBy("user id");
        return repository.save(userRole);
    }

    @Override
    public UserRole update(UserRole user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
