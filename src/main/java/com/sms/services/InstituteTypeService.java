package com.sms.services;

import com.sms.documents.InstituteType;
import org.springframework.data.domain.Pageable;

public interface InstituteTypeService {

    Iterable<InstituteType> findAllInstituteType(Pageable pageable);

    InstituteType findInstituteTypeById(String id);

    InstituteType findInstituteTypeByName(String type);

    InstituteType save(InstituteType user);

    InstituteType update(InstituteType user);

    Boolean delete(String id);
}
