package com.sms.services;

import com.sms.documents.Institute;
import org.springframework.data.domain.Pageable;

public interface InstituteService {

    Iterable<Institute> findAllInstitute(Pageable pageable);

    Institute findInstituteById(String id);

    Institute findInstituteByName(String name);

    Institute save(Institute institute);

    Institute update(Institute institute);

    Boolean delete(String id);
}
