package com.sms.services;

import com.sms.documents.UserRole;
import org.springframework.data.domain.Pageable;

public interface UserRoleService {

    Iterable<UserRole> findAllUserRole(Pageable pageable);

    UserRole findUserRoleById(String id);

    UserRole findUserRoleByName(String uRole);

    UserRole save(UserRole uRole);

    UserRole update(UserRole uRole);

    Boolean delete(String id);
}
