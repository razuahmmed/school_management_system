package com.sms.services;

import com.sms.documents.InstituteStatus;
import org.springframework.data.domain.Pageable;

public interface InstituteStatusService {

    Iterable<InstituteStatus> findAllInstitute(Pageable pageable);

    InstituteStatus findInstituteById(String id);

    InstituteStatus findInstituteByName(String name);

    InstituteStatus save(InstituteStatus status);

    InstituteStatus update(InstituteStatus status);

    Boolean delete(String id);
}
