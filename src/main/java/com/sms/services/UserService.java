package com.sms.services;

import com.sms.documents.User;
import org.springframework.data.domain.Pageable;

public interface UserService {

    Iterable<User> findAllUser(Pageable pageable);

    User findUserById(String id);

    User findUserByUserName(String uName);

    User findUserByPhone(String phone);

    User save(User user);

    User update(User user);

    Boolean delete(String id);
}
